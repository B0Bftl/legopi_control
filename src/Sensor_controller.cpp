//
// @author Lukas Schneider, 18.03.2019.
//
#include "Sensor_controller.h"

namespace LegoPi_Control
{
	/**
	 * initialize all nodes, read config
	 */
	Sensor_controller::Sensor_controller() : Controller() {

		ros::NodeHandle _nh("~"); // to get private parameters

		int dir_buffer_size;

		_nh.getParam("distance_close", distanceThreshold_Close);
		_nh.getParam("distance_medium", distanceThreshold_Medium);
		_nh.getParam("change_turn_prob", changeTurnProbability);
		_nh.getParam("direction_buffer_size", dir_buffer_size);

		randomBernoulli = std::bernoulli_distribution(changeTurnProbability);
		direction_buffer =  boost::circular_buffer<LegoPi_Control::Direction>(dir_buffer_size);

		ir_sub = nh.subscribe<sensor_msgs::Range>("/distance", 1, &Sensor_controller::ir_callback, this);
		touch_sub = nh.subscribe<std_msgs::Bool>("/touch", 1, &Sensor_controller::touch_callback, this);
		state_sub = nh.subscribe<std_msgs::Int8>("/orb_slam2/tracking_state", 1, &Sensor_controller::state_callback, this);

		currentTurnDirection = LegoPi_Control::Direction::right;

		ROS_INFO("Sensor Controller Initialized");

	}

	/**
	 * process SLAM messages. If Track lost, go into reverse_mode, tracing your own way back
	 * @param msg from SLAM
	 */
	void Sensor_controller::state_callback(const std_msgs::Int8::ConstPtr &msg) {

		switch ((*msg).data)
		{
			case LegoPi_Control::SLAMTrackingState::NOT_INITIALIZED:
				if(previous_State == LegoPi_Control::SLAMTrackingState::OK)
					ROS_INFO("Track Lost soon after initialization. Trying to reinitialize.");
				else
					ROS_INFO("Trying to initialize map...");
				break;
			case LegoPi_Control::SLAMTrackingState::LOST:
				if(previous_State == LegoPi_Control::SLAMTrackingState::OK)
				{
					ROS_INFO("Track Lost. Going into reverse mode.");
					reverse_mode = true;
				}
				break;
			case LegoPi_Control::SLAMTrackingState::OK:
				switch(previous_State)
				{
					case SLAMTrackingState::NOT_INITIALIZED:
						ROS_INFO("Initialization Done.");
						break;
					case LegoPi_Control::SLAMTrackingState::LOST:
						ROS_INFO("Track found after being Lost. Reverse mode off.");
						reverse_mode = false;
						break;
					default:
						ROS_INFO("Track found");
				}
				break;
			default:
				ROS_INFO("SLAM State changed from %i to %i.", previous_State ,(*msg).data);
		}
		previous_State = (*msg).data;
	}

	/**
	 * process distance sensor, issue appropriate steering commands
	 * @param msg from sensor
	 */
	void Sensor_controller::ir_callback(const sensor_msgs::Range::ConstPtr& msg) {

		if (touchSensorPriority) {
			ROS_INFO("Return from IR because of Touch Priority");
			return;
		}

		if(reverse_mode) {
			runReverse();
			return;
		}

		LegoPi_Control::Distance distance = getDistance((*msg).range);
		LegoPi_Control::Direction newDirection;

		switch (distance)
		{
			case LegoPi_Control::Distance::Close:
				newDirection = LegoPi_Control::Direction::backward;
				break;

			case LegoPi_Control::Distance::Medium:

				if(getTurnDirection() == LegoPi_Control::Direction::right) {
					newDirection = LegoPi_Control::Direction::forward_right;
				} else{
					newDirection = LegoPi_Control::Direction::forward_left;
				}
				break;

			case LegoPi_Control::Distance::Far:
				newDirection =  LegoPi_Control::Direction::forward;
				break;
			default:
				newDirection = LegoPi_Control::Direction::stop;
		}

		publishDirection(newDirection);

	}

	/**
	 * process touch sensor messages
	 * If pressed, take priority over any other control method and go into reverse (not reverse_mode!)
	 * @param msg from touch sensor
	 */
	void Sensor_controller::touch_callback(const std_msgs::Bool::ConstPtr& msg) {

		if ((*msg).data == true) {
			touchSensorPriority = true;
			publishDirection(LegoPi_Control::Direction::stop);
			touchCount = 0;
			ROS_INFO("Touch Sensor pressed.");
			LegoPi_Control::Direction newDirection;
			if (currentTurnDirection == LegoPi_Control::Direction::right) {
				newDirection = LegoPi_Control::Direction::backward_right;
			} else {
				newDirection = LegoPi_Control::Direction::backward_left;
			}

			publishDirection(newDirection);

		}
		else if (touchSensorPriority) {
			if (touchCount >= 20) {
				touchSensorPriority = false;
				ROS_INFO("Touch Priority released");
			} else {
				touchCount++;
				LegoPi_Control::Direction newDirection;
				if (currentTurnDirection == LegoPi_Control::Direction::right) {
					newDirection = LegoPi_Control::Direction::backward_right;
				} else {
					newDirection = LegoPi_Control::Direction::backward_left;
				}
				publishDirection(newDirection);
			}
		}

	}

	/**
	 * Interpret numerical range value based on defined thresholds
	 * @param range, sensor measurement
	 * @return interpretation to Close, Medium or Far
	 */
	LegoPi_Control::Distance Sensor_controller::getDistance(float range) {

		if (range <= distanceThreshold_Close) {
			ROS_INFO("IR detect: Close");
			return LegoPi_Control::Distance::Close;
		}
		else if (range <= distanceThreshold_Medium)
		{
			ROS_INFO("IR detect: Medium");
			return LegoPi_Control::Distance::Medium;
		}
		else
		{
			ROS_INFO("IR detect: Far");
			return LegoPi_Control::Distance::Far;
		}

	}

	/**
	 * add direction to reverse buffer
	 * @param direction to add
	 */
	void Sensor_controller::addToHistory(LegoPi_Control::Direction direction) {
		if (!reverse_mode)
			direction_buffer.push_back(direction);
	}

	/**
	 * perform reverse direction from buffer
	 * set reverse_mode false if buffer empty
	 */
	void Sensor_controller::runReverse() {
		if (!direction_buffer.empty()) {
			control_pub.publish(Message_Generator::generate_reverse_Msg(direction_buffer.back()));
			direction_buffer.pop_back();
		} else {
			reverse_mode = false;
			ROS_INFO("Stopped reverse mode, buffer empty.");
		}
	}

	/**
	 * publish direction method to configured topic
	 * @param direction to publish
	 */
	void Sensor_controller::publishDirection(LegoPi_Control::Direction direction) {
		if(!reverse_mode)
			addToHistory(direction);

		control_pub.publish(Message_Generator::generate_Msg(direction));
	}

	/**
	 * randomly change direction based on previously defined probability
	 * @return
	 */
	LegoPi_Control::Direction Sensor_controller::getTurnDirection() {

		if(getRandomBoolean()) {
			ROS_INFO("Changing turn direction");
			if (currentTurnDirection == LegoPi_Control::Direction::right)
				currentTurnDirection = LegoPi_Control::Direction::left;
			else
				currentTurnDirection = LegoPi_Control::Direction::right;
		}
		return currentTurnDirection;
	}

	/**
	 * get random boolean, true with previously defined probability
	 * @return
	 */
	bool Sensor_controller::getRandomBoolean()  // probability between 0.0 and 1.0
	{
		return randomBernoulli(rand_engine);
	}

}

