//
// @author Lukas Schneider, 18.03.2019.
//

#include "Controller.h"


LegoPi_Control::Controller::Controller() {

	control_pub = nh.advertise<geometry_msgs::Twist>("cmd_vel",1, false);
	ros::NodeHandle _nh("~"); // to get private parameters

	float _speedAngular;
	float _speedLinear;

	_nh.getParam("speed_angular", _speedAngular);
	_nh.getParam("speed_linear", _speedLinear);

	Message_Generator::setSpeedLinear(_speedLinear);
	Message_Generator::setSpeedAngular(_speedAngular);

}