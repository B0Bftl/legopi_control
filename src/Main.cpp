//
// @author Lukas Schneider, 18.03.2019.
//
#include "ros/ros.h"
#include "rosgraph_msgs/Log.h"
#include "ros/console.h"

#include "std_msgs/Bool.h"
#include "geometry_msgs/Twist.h"
#include "sensor_msgs/Range.h"
#include "enums.h"
#include <chrono>
#include <sstream>
#include <thread>

#include "Sensor_controller.h"


int main(int argc, char **argv)
{
	ros::init(argc, argv, "legopi_control");

	ros::NodeHandle nh;
	ros::NodeHandle _nh("~"); //  to get private params

	std::string control_mode;
	LegoPi_Control::Sensor_controller sensor_controller;

	_nh.getParam("control_mode", control_mode);

	if (control_mode == "sensor")
	{
		ROS_INFO("Starting Sensor control node");
		sensor_controller = LegoPi_Control::Sensor_controller();
	} else {
		ROS_ERROR("Control mode not set correctly");
		ros::shutdown();
	}

	int rate;
	_nh.getParam("loop_rate", rate);
	ros::Rate loop_rate(rate);

	while (ros::ok())
	{
		loop_rate.sleep();
		ros::spinOnce();
	}

}

