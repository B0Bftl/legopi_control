//
// @author Lukas Schneider, 18.03.2019.
//

#include "Message_Generator.h"

namespace LegoPi_Control {

	float Message_Generator::speedLinear = 0.2;
	float Message_Generator::speedAngular = 0.4;

	/**
	 * generate message with given speed and direction
	 */
	geometry_msgs::Twist Message_Generator::generate_Msg(LegoPi_Control::Direction direction, float speed) {

		geometry_msgs::Twist msg;

		switch (direction) {
			case LegoPi_Control::Direction::forward:
				msg.linear.x = (speed > 0) ? speed : speedLinear;
				msg.linear.y = 0;
				msg.linear.z = 0;

				msg.angular.x = 0;
				msg.angular.y = 0;
				msg.angular.z = 0;
				break;

			case LegoPi_Control::Direction::right:
				msg.linear.x = 0;
				msg.linear.y = 0;
				msg.linear.z = 0;

				msg.angular.x = 0;
				msg.angular.y = 0;
				msg.angular.z = (speed > 0) ? speed : speedAngular;
				break;

			case LegoPi_Control::Direction::left:
				msg.linear.x = 0;
				msg.linear.y = 0;
				msg.linear.z = 0;

				msg.angular.x = 0;
				msg.angular.y = 0;
				msg.angular.z = (speed > 0) ? -speed : -speedAngular;
				break;

			case LegoPi_Control::Direction::backward:
				msg.linear.x = (speed > 0) ? -speed : -speedLinear;
				msg.linear.y = 0;
				msg.linear.z = 0;

				msg.angular.x = 0;
				msg.angular.y = 0;
				msg.angular.z = 0;
				break;

			case LegoPi_Control::Direction::forward_right:
				msg.linear.x = (speed > 0) ? speed / 2 : speedLinear / 2;
				msg.linear.y = 0;
				msg.linear.z = 0;

				msg.angular.x = 0;
				msg.angular.y = 0;
				msg.angular.z = (speed > 0) ? speed : speedAngular;
				break;

			case LegoPi_Control::Direction::forward_left:
				msg.linear.x = (speed > 0) ? speed / 2 : speedLinear / 2;
				msg.linear.y = 0;
				msg.linear.z = 0;

				msg.angular.x = 0;
				msg.angular.y = 0;
				msg.angular.z = (speed > 0) ? -speed : -speedAngular;
				break;

			case LegoPi_Control::Direction::backward_left:
				msg.linear.x = (speed > 0) ? -speed / 2 : -speedLinear / 2;
				msg.linear.y = 0;
				msg.linear.z = 0;

				msg.angular.x = 0;
				msg.angular.y = 0;
				msg.angular.z = (speed > 0) ? -speed : -speedAngular;
				break;

			case LegoPi_Control::Direction::backward_right:
				msg.linear.x = (speed > 0) ? -speed / 2 : -speedLinear / 2;
				msg.linear.y = 0;
				msg.linear.z = 0;

				msg.angular.x = 0;
				msg.angular.y = 0;
				msg.angular.z = (speed > 0) ? speed : speedAngular;
				break;

			case LegoPi_Control::Direction::stop:
			default:
				msg.linear.x = 0;
				msg.linear.y = 0;
				msg.linear.z = 0;

				msg.angular.x = 0;
				msg.angular.y = 0;
				msg.angular.z = 0;
				break;
		}
		logDirectionInfo(direction);
		return msg;

	}

	/**
	 * generate message with given speed to the opposite of the given direction
	 */
	geometry_msgs::Twist Message_Generator::generate_reverse_Msg(LegoPi_Control::Direction direction, float speed) {
		switch(direction) {
			case LegoPi_Control::Direction::forward:
				return generate_Msg(LegoPi_Control::Direction::backward, speed);
			case Direction::forward_right:
				return generate_Msg(LegoPi_Control::Direction::backward_left, speed);
			case Direction::forward_left:
				return generate_Msg(LegoPi_Control::Direction::backward_right, speed);
			case Direction::right:
				return generate_Msg(LegoPi_Control::Direction::left, speed);
			case Direction::left:
				return generate_Msg(LegoPi_Control::Direction::right, speed);
			case Direction::backward:
				return generate_Msg(LegoPi_Control::Direction::forward, speed);
			case Direction::backward_right:
				return generate_Msg(LegoPi_Control::Direction::forward_left, speed);
			case Direction::backward_left:
				return generate_Msg(LegoPi_Control::Direction::forward_right, speed);
			case Direction::stop:
			default:
				return generate_Msg(LegoPi_Control::Direction::stop, speed);
		}
	}

	/**
	 * log direction to rosout
	 */
	void Message_Generator::logDirectionInfo(LegoPi_Control::Direction direction) {

		switch (direction) {

			case LegoPi_Control::Direction::forward:
				ROS_INFO("Direction: forward");
				break;
			case LegoPi_Control::Direction::forward_right:
				ROS_INFO("Direction: forward_right");
				break;
			case LegoPi_Control::Direction::forward_left:
				ROS_INFO("Direction: forward_left");
				break;
			case LegoPi_Control::Direction::right:
				ROS_INFO("Direction: right");
				break;
			case LegoPi_Control::Direction::left:
				ROS_INFO("Direction: left");
				break;
			case LegoPi_Control::Direction::backward:
				ROS_INFO("Direction: backward");
				break;
			case LegoPi_Control::Direction::backward_right:
				ROS_INFO("Direction: backward_right");
				break;
			case LegoPi_Control::Direction::backward_left:
				ROS_INFO("Direction: backward_left");
				break;
			case LegoPi_Control::Direction::stop:
				ROS_INFO("Direction: stop");
				break;

			default:
				ROS_INFO("Unknown direction");
				break;
		}

	}

	void Message_Generator::setSpeedLinear(float _speedLinear) {
		Message_Generator::speedLinear = _speedLinear;
	}

	void Message_Generator::setSpeedAngular(float _speedAngular) {
		Message_Generator::speedAngular = _speedAngular;
	}


};