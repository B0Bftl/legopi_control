//
// @author Lukas Schneider, 18.03.2019.
//

#ifndef LEGOPI_CONTROL_SENSOR_CONTROLLER_H
#define LEGOPI_CONTROL_SENSOR_CONTROLLER_H
#include "enums.h"
#include "Message_Generator.h"
#include "Controller.h"
#include "std_msgs/Int8.h"
#include <boost/circular_buffer.hpp>
#include <random>

namespace LegoPi_Control
{

	class Sensor_controller : public Controller {

	public:
		Sensor_controller();
	private:

		void ir_callback(const sensor_msgs::Range::ConstPtr& msg);
		void touch_callback(const std_msgs::Bool::ConstPtr& msg);
		void state_callback(const std_msgs::Int8::ConstPtr& msg);
		void publishDirection(LegoPi_Control::Direction direction);

		bool getRandomBoolean();
		LegoPi_Control::Direction getTurnDirection();
		LegoPi_Control::Distance getDistance(float range);

		void addToHistory(LegoPi_Control::Direction direction);
		void runReverse();

		ros::Subscriber ir_sub;
		ros::Subscriber touch_sub;
		ros::Subscriber state_sub;

		boost::circular_buffer<LegoPi_Control::Direction> direction_buffer;

		bool touchSensorPriority = false;
		bool reverse_mode = false;
		int touchCount = 0;
		int previous_State = -2;
		float distanceThreshold_Close;
		float distanceThreshold_Medium;
		float changeTurnProbability;
		LegoPi_Control::Direction currentTurnDirection;

		std::bernoulli_distribution randomBernoulli;
		std::knuth_b rand_engine;

	};



}

#endif //LEGOPI_CONTROL_SENSOR_CONTROLLER_H
