//
// @author Lukas Schneider, 18.03.2019.
//

#ifndef LEGOPI_CONTROL_MESSAGE_GENERATOR_H
#define LEGOPI_CONTROL_MESSAGE_GENERATOR_H

#include "enums.h"
#include "geometry_msgs/Twist.h"
#include "ros/ros.h"
#include "rosgraph_msgs/Log.h"
#include "ros/console.h"

#include "std_msgs/Bool.h"
#include "geometry_msgs/Twist.h"
#include "sensor_msgs/Range.h"

namespace LegoPi_Control{

	class Message_Generator {

	public:
		geometry_msgs::Twist static generate_Msg(LegoPi_Control::Direction direction, float speed = 0);
		geometry_msgs::Twist static generate_reverse_Msg(LegoPi_Control::Direction direction, float speed = 0);
		void static setSpeedLinear(float _speedLinear);
		void static setSpeedAngular(float _speedAngular);

	private:
		void static logDirectionInfo(LegoPi_Control::Direction direction);
		static float speedLinear;
		static float speedAngular;
	};


}


#endif //LEGOPI_CONTROL_MESSAGE_GENERATOR_H
