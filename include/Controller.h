//
// @author Lukas Schneider, 18.03.2019.
//

#ifndef LEGOPI_CONTROL_CONTROLLER_H
#define LEGOPI_CONTROL_CONTROLLER_H

#include "ros/ros.h"
#include "Message_Generator.h"

namespace LegoPi_Control {

	class Controller {

	public:
		Controller();
		ros::NodeHandle nh;
		ros::Publisher control_pub;

	};


}

#endif //LEGOPI_CONTROL_CONTROLLER_H
