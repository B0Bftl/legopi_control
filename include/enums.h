//
// @author Lukas Schneider, 18.03.2019.
//

#ifndef PROJECT_DIRECTION_H
#define PROJECT_DIRECTION_H

namespace LegoPi_Control {

	enum class Direction {
		forward,
		forward_right,
		forward_left,
		right,
		left,
		backward,
		backward_right,
		backward_left,
		stop

	};

	enum class Distance {
		Close,
		Medium,
		Far
	};

	enum SLAMTrackingState{
		SYSTEM_NOT_READY=-1,
		NO_IMAGES_YET=0,
		NOT_INITIALIZED=1,
		OK=2,
		LOST=3
	};

}
#endif //PROJECT_DIRECTION_H
