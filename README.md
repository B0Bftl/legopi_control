
* clone into initialized catkin_ws
* setup ros environment by ```source devel/setup.bash``` 
* compile with ```catkin_make``` (has to be run from catkin_ws root directory)
* start ROS with ```roscore```
* start node with ``` rosrun legopi_control legopi_control```
* check sent messages run ```rostopic echo /cmd_vel```
